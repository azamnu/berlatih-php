<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mengubah Huruf</title>
</head>
<body>
    <h3>Mengubah Huruf</h3>
        <?php
        function ubah_huruf($string){
        //kode di sini
        $ubah = strtr($string, 
        'abcdefghijklmnopqrstuvwxyz',
        'bcdefghijklmnopqrstuvwxyza');
        return $ubah;
        }

        // TEST CASES
        echo "wow => ". ubah_huruf('wow'). "<br>"; // xpx
        echo "developer => ".ubah_huruf('developer'). "<br>"; // efwfmpqfs
        echo "laravel => ".ubah_huruf('laravel'). "<br>"; // mbsbwfm
        echo "keren => ".ubah_huruf('keren'). "<br>"; // lfsfo
        echo "semangat => ".ubah_huruf('semangat'). "<br>"; // tfnbohbu

        ?>
    
</body>
</html>