<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Menentukan Nilai</title>
</head>
<body>
        <?php
        echo "<h3>Function Menentukan Nilai</h3>";
        function tentukan_nilai($number)
        {
            //  kode disini
            $nilai="";
            if ($number >= 85 && $number <=100){
                $nilai = "Sangat Baik";
            }
            elseif ($number >= 70 && $number <=85){
                $nilai = "Baik";
            }
            elseif ($number >= 60 && $number <=70){
                $nilai = "Cukup";
            }
            else {
                $nilai = "Cukup";
            }
            return $nilai;
        }

        //TEST CASES
        echo "nilai 98: ". tentukan_nilai(98) . "<br>"; //Sangat Baik
        echo "nilai 76: ". tentukan_nilai(76) . "<br>"; //Baik
        echo "nilai 67: ". tentukan_nilai(67) . "<br>"; //Cukup
        echo "nilai 43: ". tentukan_nilai(43) . "<br>"; //Kurang
        ?>
</body>
</html>