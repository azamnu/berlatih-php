<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tukar Huruf Besar dan Kecil</title>
</head>
<body>
    <h3>Tukar Huruf Besar dan Kecil</h3>
    <?php
    function tukar_besar_kecil($string){
    //kode di sini
    $ubah = strtr($string, 
           'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz',
           'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
    return $ubah;
    }

    // TEST CASES
    echo "Hello World => ". tukar_besar_kecil('Hello World'). "<br>"; // "hELLO wORLD"
    echo "I aM aLAY => ". tukar_besar_kecil('I aM aLAY'). "<br>"; // "i Am Alay"
    echo "My Name is Bond!! => ". tukar_besar_kecil('My Name is Bond!!'). "<br>"; // "mY nAME IS bOND!!"
    echo "IT sHOULD bE me => ". tukar_besar_kecil('IT sHOULD bE me'). "<br>"; // "it Should Be ME"
    echo "001-A-3-5TrdYW => ". tukar_besar_kecil('001-A-3-5TrdYW'). "<br>"; // "001-a-3-5tRDyw"

    ?>
    
</body>
</html>